--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 12.0

-- Started on 2019-10-18 02:03:06

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 201 (class 1259 OID 17796)
-- Name: aluno_turma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aluno_turma (
    id bigint NOT NULL,
    aluno_id integer,
    turma_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.aluno_turma OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 17770)
-- Name: alunos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.alunos (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    serie integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.alunos OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 17768)
-- Name: alunos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.alunos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alunos_id_seq OWNER TO postgres;

--
-- TOC entry 2222 (class 0 OID 0)
-- Dependencies: 196
-- Name: alunos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.alunos_id_seq OWNED BY public.alunos.id;


--
-- TOC entry 200 (class 1259 OID 17794)
-- Name: alunos_turmas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.alunos_turmas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alunos_turmas_id_seq OWNER TO postgres;

--
-- TOC entry 2223 (class 0 OID 0)
-- Dependencies: 200
-- Name: alunos_turmas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.alunos_turmas_id_seq OWNED BY public.aluno_turma.id;


--
-- TOC entry 191 (class 1259 OID 17731)
-- Name: escolas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.escolas (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    descricao character varying(100) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.escolas OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 17729)
-- Name: escolas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.escolas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.escolas_id_seq OWNER TO postgres;

--
-- TOC entry 2224 (class 0 OID 0)
-- Dependencies: 190
-- Name: escolas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.escolas_id_seq OWNED BY public.escolas.id;


--
-- TOC entry 186 (class 1259 OID 17703)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 17701)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2225 (class 0 OID 0)
-- Dependencies: 185
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 199 (class 1259 OID 17778)
-- Name: notas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notas (
    id bigint NOT NULL,
    nota integer NOT NULL,
    data date NOT NULL,
    aluno_id integer,
    turma_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.notas OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 17776)
-- Name: notas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notas_id_seq OWNER TO postgres;

--
-- TOC entry 2226 (class 0 OID 0)
-- Dependencies: 198
-- Name: notas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notas_id_seq OWNED BY public.notas.id;


--
-- TOC entry 189 (class 1259 OID 17722)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 17739)
-- Name: professors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.professors (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    disciplina character varying(100) NOT NULL,
    escola_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.professors OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 17737)
-- Name: professors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.professors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.professors_id_seq OWNER TO postgres;

--
-- TOC entry 2227 (class 0 OID 0)
-- Dependencies: 192
-- Name: professors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.professors_id_seq OWNED BY public.professors.id;


--
-- TOC entry 195 (class 1259 OID 17752)
-- Name: turmas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turmas (
    id bigint NOT NULL,
    nome character varying(100) NOT NULL,
    serie integer NOT NULL,
    escola_id integer,
    professor_id integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.turmas OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 17750)
-- Name: turmas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.turmas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turmas_id_seq OWNER TO postgres;

--
-- TOC entry 2228 (class 0 OID 0)
-- Dependencies: 194
-- Name: turmas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.turmas_id_seq OWNED BY public.turmas.id;


--
-- TOC entry 188 (class 1259 OID 17711)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 17709)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2229 (class 0 OID 0)
-- Dependencies: 187
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2056 (class 2604 OID 17799)
-- Name: aluno_turma id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno_turma ALTER COLUMN id SET DEFAULT nextval('public.alunos_turmas_id_seq'::regclass);


--
-- TOC entry 2054 (class 2604 OID 17773)
-- Name: alunos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alunos ALTER COLUMN id SET DEFAULT nextval('public.alunos_id_seq'::regclass);


--
-- TOC entry 2051 (class 2604 OID 17734)
-- Name: escolas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.escolas ALTER COLUMN id SET DEFAULT nextval('public.escolas_id_seq'::regclass);


--
-- TOC entry 2049 (class 2604 OID 17706)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2055 (class 2604 OID 17781)
-- Name: notas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notas ALTER COLUMN id SET DEFAULT nextval('public.notas_id_seq'::regclass);


--
-- TOC entry 2052 (class 2604 OID 17742)
-- Name: professors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professors ALTER COLUMN id SET DEFAULT nextval('public.professors_id_seq'::regclass);


--
-- TOC entry 2053 (class 2604 OID 17755)
-- Name: turmas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turmas ALTER COLUMN id SET DEFAULT nextval('public.turmas_id_seq'::regclass);


--
-- TOC entry 2050 (class 2604 OID 17714)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2216 (class 0 OID 17796)
-- Dependencies: 201
-- Data for Name: aluno_turma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aluno_turma (id, aluno_id, turma_id, created_at, updated_at) FROM stdin;
1	1	1	\N	\N
4	4	2	\N	\N
5	5	2	\N	\N
6	6	2	\N	\N
\.


--
-- TOC entry 2212 (class 0 OID 17770)
-- Dependencies: 197
-- Data for Name: alunos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.alunos (id, nome, serie, created_at, updated_at) FROM stdin;
1	João	1	2019-10-18 00:30:28	2019-10-18 00:30:28
4	Pedro	1	2019-10-18 00:32:02	2019-10-18 00:32:02
5	Ricardo	2	2019-10-18 00:32:26	2019-10-18 00:32:26
6	Alberto	2	2019-10-18 00:32:44	2019-10-18 00:32:44
8	Márcio da Silva	2	2019-10-18 03:51:28	2019-10-18 03:51:28
10	Fulana de Tal	2	2019-10-18 03:55:12	2019-10-18 03:55:12
14	Maria Rosa	2	2019-10-18 03:59:31	2019-10-18 03:59:31
\.


--
-- TOC entry 2206 (class 0 OID 17731)
-- Dependencies: 191
-- Data for Name: escolas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.escolas (id, nome, descricao, created_at, updated_at) FROM stdin;
1	Escola Teste	Primeira Escola adicionado pelo Tinker	2019-10-18 00:19:47	2019-10-18 00:19:47
\.


--
-- TOC entry 2201 (class 0 OID 17703)
-- Dependencies: 186
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_10_17_225941_create_escolas_table	1
4	2019_10_17_230234_create_professors_table	1
5	2019_10_17_230253_create_turmas_table	1
6	2019_10_17_230311_create_alunos_table	1
7	2019_10_17_230321_create_notas_table	1
8	2019_10_18_000236_create_alunos_turmas_table	1
\.


--
-- TOC entry 2214 (class 0 OID 17778)
-- Dependencies: 199
-- Data for Name: notas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notas (id, nota, data, aluno_id, turma_id, created_at, updated_at) FROM stdin;
1	8	2019-10-18	1	1	2019-10-18 00:41:00	2019-10-18 00:41:00
\.


--
-- TOC entry 2204 (class 0 OID 17722)
-- Dependencies: 189
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2208 (class 0 OID 17739)
-- Dependencies: 193
-- Data for Name: professors; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.professors (id, nome, disciplina, escola_id, created_at, updated_at) FROM stdin;
1	Vera Lúcia	Português	1	2019-10-18 00:22:36	2019-10-18 00:22:36
2	Valentino Rossi	Matemática	\N	2019-10-18 00:42:23	2019-10-18 00:42:23
\.


--
-- TOC entry 2210 (class 0 OID 17752)
-- Dependencies: 195
-- Data for Name: turmas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turmas (id, nome, serie, escola_id, professor_id, created_at, updated_at) FROM stdin;
1	A	1	1	1	2019-10-18 00:27:55	2019-10-18 00:27:55
2	A	1	1	2	2019-10-18 00:45:14	2019-10-18 00:45:14
\.


--
-- TOC entry 2203 (class 0 OID 17711)
-- Dependencies: 188
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2230 (class 0 OID 0)
-- Dependencies: 196
-- Name: alunos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.alunos_id_seq', 27, true);


--
-- TOC entry 2231 (class 0 OID 0)
-- Dependencies: 200
-- Name: alunos_turmas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.alunos_turmas_id_seq', 7, true);


--
-- TOC entry 2232 (class 0 OID 0)
-- Dependencies: 190
-- Name: escolas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.escolas_id_seq', 6, true);


--
-- TOC entry 2233 (class 0 OID 0)
-- Dependencies: 185
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 8, true);


--
-- TOC entry 2234 (class 0 OID 0)
-- Dependencies: 198
-- Name: notas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notas_id_seq', 1, true);


--
-- TOC entry 2235 (class 0 OID 0)
-- Dependencies: 192
-- Name: professors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.professors_id_seq', 2, true);


--
-- TOC entry 2236 (class 0 OID 0)
-- Dependencies: 194
-- Name: turmas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.turmas_id_seq', 2, true);


--
-- TOC entry 2237 (class 0 OID 0)
-- Dependencies: 187
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- TOC entry 2071 (class 2606 OID 17775)
-- Name: alunos alunos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.alunos
    ADD CONSTRAINT alunos_pkey PRIMARY KEY (id);


--
-- TOC entry 2075 (class 2606 OID 17801)
-- Name: aluno_turma alunos_turmas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno_turma
    ADD CONSTRAINT alunos_turmas_pkey PRIMARY KEY (id);


--
-- TOC entry 2065 (class 2606 OID 17736)
-- Name: escolas escolas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.escolas
    ADD CONSTRAINT escolas_pkey PRIMARY KEY (id);


--
-- TOC entry 2058 (class 2606 OID 17708)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2073 (class 2606 OID 17783)
-- Name: notas notas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notas
    ADD CONSTRAINT notas_pkey PRIMARY KEY (id);


--
-- TOC entry 2067 (class 2606 OID 17744)
-- Name: professors professors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professors
    ADD CONSTRAINT professors_pkey PRIMARY KEY (id);


--
-- TOC entry 2069 (class 2606 OID 17757)
-- Name: turmas turmas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turmas
    ADD CONSTRAINT turmas_pkey PRIMARY KEY (id);


--
-- TOC entry 2060 (class 2606 OID 17721)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2062 (class 2606 OID 17719)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2063 (class 1259 OID 17728)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 2081 (class 2606 OID 17802)
-- Name: aluno_turma alunos_turmas_aluno_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno_turma
    ADD CONSTRAINT alunos_turmas_aluno_id_foreign FOREIGN KEY (aluno_id) REFERENCES public.alunos(id) ON DELETE CASCADE;


--
-- TOC entry 2082 (class 2606 OID 17807)
-- Name: aluno_turma alunos_turmas_turma_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno_turma
    ADD CONSTRAINT alunos_turmas_turma_id_foreign FOREIGN KEY (turma_id) REFERENCES public.turmas(id) ON DELETE CASCADE;


--
-- TOC entry 2079 (class 2606 OID 17784)
-- Name: notas notas_aluno_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notas
    ADD CONSTRAINT notas_aluno_id_foreign FOREIGN KEY (aluno_id) REFERENCES public.alunos(id) ON DELETE CASCADE;


--
-- TOC entry 2080 (class 2606 OID 17789)
-- Name: notas notas_turma_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notas
    ADD CONSTRAINT notas_turma_id_foreign FOREIGN KEY (turma_id) REFERENCES public.turmas(id) ON DELETE CASCADE;


--
-- TOC entry 2076 (class 2606 OID 17745)
-- Name: professors professors_escola_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.professors
    ADD CONSTRAINT professors_escola_id_foreign FOREIGN KEY (escola_id) REFERENCES public.escolas(id) ON DELETE CASCADE;


--
-- TOC entry 2077 (class 2606 OID 17758)
-- Name: turmas turmas_escola_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turmas
    ADD CONSTRAINT turmas_escola_id_foreign FOREIGN KEY (escola_id) REFERENCES public.escolas(id) ON DELETE CASCADE;


--
-- TOC entry 2078 (class 2606 OID 17763)
-- Name: turmas turmas_professor_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turmas
    ADD CONSTRAINT turmas_professor_id_foreign FOREIGN KEY (professor_id) REFERENCES public.professors(id) ON DELETE CASCADE;


-- Completed on 2019-10-18 02:03:06

--
-- PostgreSQL database dump complete
--

