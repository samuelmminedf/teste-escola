<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $fillable = [
        'nome', 'serie'
    ];

    public function notas()
    {
        return $this->belongsTo('App\Nota');
    }

    public function turmas()
    {
        return $this->belongsToMany('App\Turma');
    }
}
