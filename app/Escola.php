<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escola extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome', 'descricao'
     ];

    public function professores() 
	{
        return $this->belongsTo('App\Professor');
    }
      
    public function turmas() 
	{
        return $this->belongsTo('App\Turma');
  	}
}
