<?php

namespace App\Http\Controllers;

use App\Escola;
use Illuminate\Http\Request;

class EscolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $escolas = Escola::all();
        return view('escola')->with('escolas', $escolas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = $request->all();
        $escola = Escola::create([
            'nome' => $data['nome'],
            'descricao' => $data['descricao']
        ]);
        $escolas = Escola::all();
        return view('escola')->with('escolas', $escolas);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Escola  $escola
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $escola = Escola::find($id);
      return view('editarEscola')->with('escola', $escola);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Escola  $escola
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $data = $request->all();
        $escola = Escola::find($data['id']);
        $escola->nome = $data['nome'];
        $escola->descricao = $data['descricao'];
        $escola->save();
        $escolas = Escola::all();
        return view('escola')->with('escolas', $escolas);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Escola  $escola
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $escola = Escola::find($id);
      if(!empty($escola)) {
        $escola=Escola::where('id',$id)->delete();
      } 
      $escolas = Escola::all();
      return view('escola')->with('escolas', $escolas);
    }
}
