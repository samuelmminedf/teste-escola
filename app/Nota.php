<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $fillable = [
        'nota', 'data', 'aluno_id', 'turma_id'
    ];

    public function alunos()
    {
        return $this->hasOne('App\Aluno');
    }

    public function turma()
    {
        return $this->hasOne('App\Turma');
    }




}
