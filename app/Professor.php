<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    protected $fillable = [
        'nome', 'disciplina','escola_id'
     ];

    public function escolas()
    {
        return $this->hasOne('App\Escola');
    }


}
