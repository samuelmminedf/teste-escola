<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turma extends Model
{
    protected $fillable = [
        'nome', 'serie', 'escola_id', 'professor_id'
    ];


    public function escolas()
    {
        return $this->hasOne('App\Escola');
    }

    public function professores()
    {
        return $this->hasOne('App\Professor');
    }

    public function notas()
    {
        return $this->belongsTo('App\Nota');
    }

    public function alunos()
    {
        return $this->belongsToMany('App\Aluno');
    }
}
