<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>CRUD Escola</title>
  </head>
  <body>

      <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <a class="navbar-brand" href="/escola">Escola Teste</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="/escola">Escola</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/aluno">Aluno</a>
              </li>
            </ul>
          </div>
        </nav>

        <div class="row">
          <br />
        </div>

        <div class="container">
          <div class="row">
              <div class="col-12">
                  <ul class="list-group">
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          id - nome
                          <span class="badge badge-primary badge-pill">Série</span>
                          Ações
                      </li>
                      @foreach ($alunos as $aluno)
                      <li class="list-group-item d-flex justify-content-between align-items-center">
                          {{ $aluno->id }} - {{ $aluno->nome }}
                          <span class="badge badge-primary badge-pill">{{ $aluno->serie }}</span>
                          <span>
                          <a href="/editarAluno/{{ $aluno->id }}">
                              <button type="button" class="btn btn-info text-right">Editar</button>
                          </a>
                          <a href="/aluno/{{ $aluno->id }}">
                              <button type="button" class="btn btn-danger text-right">Deletar</button>
                          </a>
                          </span>
                      </li>
                      @endforeach
                    </ul>
              </div>
          </div>
        </div>

        <hr />


        <div class="container">
          <div class="row">
            <div class="col-8">
               <p><b>Adicionar Aluno</b></p>
               <form method="POST" action="/aluno">
                @csrf <!-- {{ csrf_field() }} -->
                    <div class="form-group">
                      <label for="nome">Nome</label>
                      <input type="text" class="form-control" id="nome" required name="nome" placeholder="Nome do Aluno" maxlength="99">
                    </div>
                    <div class="form-group">
                      <label for="serie">Série</label>
                      <select class="custom-select" required name="serie" id="serie">
                          <option value="1">1</option>
                          <option value="2">2</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                  </form>
            </div>
          </div>
        </div>

   

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>