<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/escola', 'EscolaController@index');
Route::get('/editarEscola/{id}', 'EscolaController@show');
Route::post('/editarEscola', 'EscolaController@edit');
Route::post('/escola', 'EscolaController@create');
Route::get('/escola/{id}', 'EscolaController@destroy');

Route::get('/aluno', 'AlunoController@index');
Route::post('/aluno', 'AlunoController@create');
Route::get('/aluno/{id}', 'AlunoController@destroy');
Route::get('/editarAluno/{id}', 'AlunoController@show');
Route::post('/editarAluno', 'AlunoController@edit');
